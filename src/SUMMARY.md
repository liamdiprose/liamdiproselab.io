# Documentation Summary

- [Design](tracking/tracking.md)
    - [Requirements](tracking/requirements.md)
    - [Specifications](tracking/specifications.md)
    - [Thesis](tracking/thesis.md)
    - [NextGen](tracking/nextgen.md)

- [Alternative Applications](ch1-alternative-markets.md)
    - [Diary](ch1-01-dairy.md)
