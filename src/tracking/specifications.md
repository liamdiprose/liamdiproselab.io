# Specifications

How the requirements are met in the design.

## Location Accuracy

To achieve a Time of Arrival accurate to 10 nanoseconds, the thesis's proposed design uses multiple techniques to improve arrival-time accuracy.

### Gold Codes

Gold codes are a series of bits that make it easier to precisely detect when a transmission was received. Gold codes are special in that they create a sharp peak when autocorrelated. Autocorrelation is essentially a math operation that reports how much waveform overlaps with another version of itself. Receivers autocorrelate the radio input constantly with a gold code. When a transmitter sends that same gold code over the air, the receiver will see the transmission as a sharp peak. The time of this sharp peak directly corresponds to the time-of-arrival of the transmission, and it's sharpness corresponds to the accuracy.


### Peak Interpolation

Due to the SDR taking measurements at discrete time intervals, sometimes the true peak from autocorrelation will occur inbetween two measurements.
In this case, the autocorrelation peak will appear truncated. The thesis uses a technique that can predict where the peak is based off the two sides, called interpolation.


### Spread-Spectrum

Normally, radio communication designs try to use as little bandwidth as possible. This is so more people can transmit in the same band. 
Spread-Spectrum methods purposely spread it out over more bandwidth. Taking up more bandwidth can help receivers to detect the transmission in noise and even improves the accuracy of time-of-arrival estimates.

Transmitter chips that use spectrum spreading need to be able to output more bandwidth (1 MHz was used in the thesis).


### Time Synchronisation

- Receivers need to have the same time frame
- Correct receiver clocks every X minutes with beacons with known locations
