# Radio Tracking Requirements


## Long Range

The system should work in a 800 by 800 meter paddock.

This requirement means the transmitter should be detectable over 1.1 km away (by law of Pythagoras).


## Location Accuracy

The applications proposed for the tracking system require a location accuracy of around 5 meters.
The system calculates the transmitter's location by comparing the times of arrival of its radio transmission, 
which travels at the speed of light, approximately 300,000,000 meters per second.

To achieve 5 meter location accuracy, the receivers need to measure the time of arrival of the transmission to 
an accuracy around 10 nanoseconds.

## Ear-Tag Maintenance

The maintenance of ear-tags is very difficult once the tag is on the animal, so the eartags should not require maintenance (including battery replacement) for the entire lifetime of the sheep.

A sheep's lifetime is around 6 years.
